Quiz 
1. **Calculate the number of 25 fps FullHD RGB video channels that can be simultaneously streamed through the 1 Gbit Ethernet LAN with 10x video compression ratio. Round the answer down to nearest integer**
***Hint: Calculate the throughput of 1 Gbit LAN and size of FullHD***
*Answer:* ???


2. **Which of these metrics may serve as performance measures for optical flow estimation?**
- [ ] Detection Error Tradeoff curve
- [x] Angular Error
- [x] Endpoint Error
- [ ] Average Precision
- [ ] Correlation between two vectors

3. **Сalculate Endpoint Error for two motion vector: Ground Truth = [1,1], Estimated = [2,0]. Specify 3 digits after comma.**
*Answer:* 0.785
*Solution/Comments*: Angle between this 2 vectors is 45 degrees or pi/4 in radians. 
Straightforward computation:
$angle=arccos(\frac{\mathbf a \cdot \mathbf b}{\left\| \mathbf a \right\| \, *\left\| \mathbf b \right\|})=arccos(\frac{1*2+1*0}{\sqrt2*2})=arccos(\frac{\sqrt2}{2})=0.785$

4. **In visual object tracking task, what does Equivalent Filter Operations metric measure?**
- [x] The time required for tracking algorithm to run compared to the time required for image filtering operation to run
- [ ] The number of feature maps required to produce an appropriate robustness for the tracker
- [ ] The number of convolutions required to achieve a specified tracking quality
5. **Which of these are types of errors that a multiple object tracker can suffer?**
- [x] False negative error
- [x] False positive error
- [x] ID switch
- [ ] False acceptance error
- [ ] False coverage error
- [ ] Mean absolute error
6. **Compute MOTA score for a multiple object tracking method, which produces 530 detections, 50 false positive errors, 20 false negative errors, 30 ID switches on a dataset with 200 frames and 500 ground truth detections and 300 trajectories? Use at most one decimal precision places.**
*Answer:* 0.8
*Solution/Comments:* To answer on this question you should close eyes to some excessively information about count of trajectory and frames. It isn't obvious, but to get correct answer you should also assume that all ground truth detections are not only truth as results of detection algorithm, but also linked in trajectories in a proper way. So in a problem statment we have already summed data about errors. 
To get more details I recommend to look at [this](https://cvhci.anthropomatik.kit.edu/~stiefel/papers/ECCV2006WorkshopCameraReady.pdf) paper (paragraph 2.2)
$$M O T A=1-\frac{\sum_{t}\left(FN_{t}+FP_{t}+IDSW_{t}\right)}{\sum_{t} GT_{t}}$$
where $GT_{t}$ - the number of objects present at time t, $FN_{t}$ - the number of misses for time t, $FR_{t}$ - number of false positives for time t, $IDSW_{t}$ - number of mismatches for time t.
In this case:
$M O T A=1-\frac{20+50+30}{500}=0.8$
7. **What is the effect of using re-identification on the tracking errors in multiple-object tracking methods?**
- [ ] False positives are decreased
- [x] False negatives are decreased
- [x] ID switches are reduced
- [x] Number of Mostly Tracked is increased
- [ ] Number of Mostly Lost is increased
*Solution/Comments:* As far as we add some positive perdiction FP couldn't decrease (some of new positive predictions could be wrong), but FN could (some of new positive predictions would be right). Also where would be some hope that we would continiously track whole trajectory, so ID switches would reduced, also number of Mostly Tracked would increased, backwards to Mostly Tracked(some of trajectory with less 20% of detection would be updated).
8. **Select correct statements regarding action classification.**
- [ ] It is easy for convolutional neural network to extract and use motion information automatically, when applied to whole video volume.
- [ ] In dense trajectories with CNN features, point neighbourhoods are cropped from frames along the trajectory, concatenated into space-time volume along the trajectory, and then supplied to CNN for feature computation.
- [x] To localize actions in videos we usually detect and track relevant objects first, and then apply action classification in a temporal window along the track.
- [x] By explicit consideration of motion information in form of optical flow maps, point and keypoint trajectories, we can currently improve the performance of action recognition.
*Solution/Comments:* First statement is obviously wrong as for ourdays. 