#Image segmentation and synthesis
**1. Which of the following is an operation, not a task in computer vision?**
- [x] Max-pooling
- [ ] Instance segmentation
- [x] Perspective projection
- [x] Image convolution
- [ ] Object detection
- [x] Gradient computation
**2. For a 3-class semantic segmentation problem, how many numbers must an algorithm output for a 640x480 image?**
*Answer:* **921600** 
**3. Why is SLIC algorithm better suited to the image oversegmentation task than k-means method?**
- [x] It limits distance between pixels by a certain threshold, utilizing the notion of hard spatial neighbourhood
- [x] It is more computationally efficient because segment sizes are bounded, limiting the number of pixels examined at each iteration
- [ ] It utilizes a more robust distance metric, rather than simple Euclidean distance used in k-means method
- [ ] Unlike k-means, SLIC is a supervised learning method and thus can use labels to improve segmentation
**4. What is the goal of the unpooling operation?**
- [ ] To undo channel concatenation by decreasing the number of convolutional feature maps
- [x] To undo pooling by outputting an image with larger resolution (i.e., pixels in spatial directions)
- [ ] To undo convolution by applying the transposed convolution
- [ ] To help backpropagate errors by introducing sparse convolutions
**5. In unpooling, how do we approximate the inverse of the non-invertible max-pooling operation?**
- [x] We output maximal values at their respective indexes (called max location switches) and place zeroes elsewhere
- [ ] We do bilinear interpolation to compute the output
- [ ] We use ‘bed of nails’: output the maximal values in the top left corner and zeros elsewhere
**6. What is a Gram matrix in linear algebra?**
- [x] A matrix produced by computing dot product between two sets of vectors
- [ ] A matrix of feature activations in a CNN
- [ ] A confusion matrix of CNN
- [ ] A positive-semidefinite matrix used to generate random numbers from a Gaussian distribution
**7. What makes a good generator for a GAN model?**
- [ ] It achieves superior performance in generating Gaussian mixtures
- [x] It produces data that is hard to distinguish from real
- [ ] It produces nicely looking images
